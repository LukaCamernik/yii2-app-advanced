<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm-email', 'token' => $user->email_confirm_token]);
?>
Hello,

You have registered with <?= $user->email ?>.

Please click or copy the link below to activate your account.

<?= Html::encode($confirmLink) ?>

