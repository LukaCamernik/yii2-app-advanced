<?php

return [
    '' => 'site/index',
    '<controller:\w+>' => '<controller>/index',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
];