<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
Hello,

We have received password reset request for <?= $user->email ?>.

If you received a password reset email but didn't request it, feel free to disregard.

Follow the link below to reset your password:

<?= $resetLink ?>
