<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm-email', 'token' => $user->email_confirm_token]);
?>
<div class="password-reset">
    <p>Hello,</p>

    <p>You have registered with <?= $user->email ?>.</p>

    <p>Please click or copy the link below to activate your account.</p>

    <p><?= Html::a(Html::encode($confirmLink), $confirmLink) ?></p>
</div>
